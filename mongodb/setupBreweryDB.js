//load the Client interface
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var fs = require('fs');

// Connection details
var mongoServiceName = process.env.DATABASE_SERVICE_NAME,
      mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'] || '127.0.0.1',
      mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'] || '27017',
      mongoDatabase = process.env[mongoServiceName + '_DATABASE'] || 'brewery',
      mongoPassword = process.env[mongoServiceName + '_PASSWORD'] || 'developer',
      mongoUser = process.env[mongoServiceName + '_USER'] || 'developer';

if (mongoHost && mongoPort && mongoDatabase) {
  mongoURLLabel = mongoURL = 'mongodb://';
  if (mongoUser && mongoPassword) {
    mongoURL += mongoUser + ':' + mongoPassword + '@';
  }
  // Provide UI label that excludes user id and pw
  mongoURLLabel += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
  mongoURL += mongoHost + ':' +  mongoPort + '/' + mongoDatabase;
}

// Use connect method to connect to the server
console.log("Connecting to MongoDB : %s ...", mongoURL);
MongoClient.connect(mongoURL, function(err, db) {
  assert.equal(null, err);
  console.log("Connected successfully to MongoDB : %s!", mongoURL);
  var colName = 'barrels';
  var barrelsCol = db.collection(colName);

  fs.readFile('json/barrels.json', 'utf8', function (err, data) {
    if (err) {
      throw err;
    } else {
      var json = JSON.parse(data);

      barrelsCol.deleteMany({},function(err, doc) {
        if(err) {
          throw err;
        } else {
          console.log("Collection '%s' has been dropped!", colName);
        };
      });

      barrelsCol.insert(json, function(err, doc) {
        if(err) {
          throw err;
        } else {
          console.log("Collection '%s' has been initialized!", colName);
        };

        //Close connection
        db.close();
      });
    };
  });
});
