#!/bin/bash

oc cluster up

oc new-project brewery \
    --description="This is a demo with JDV/JDG" \
    --display-name="Brewery Demo"

oc new-app --name="brewerydb" -e \
    MONGODB_USER=developer,MONGODB_PASSWORD=developer,MONGODB_DATABASE=brewery,MONGODB_ADMIN_PASSWORD=developer \
    centos/mongodb-26-centos7

