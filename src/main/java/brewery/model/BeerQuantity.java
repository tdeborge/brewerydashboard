package brewery.model;

public enum BeerQuantity {
	HALF_A_PINT, PINT
}
