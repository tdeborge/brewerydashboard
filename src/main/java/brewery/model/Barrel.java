package brewery.model;

import java.io.Serializable;

import org.infinispan.protostream.annotations.ProtoField;

/**
 * Represents a barrel in the brewery. Barrel Objects are stored in the cache.
 * 
 * @author mcouliba
 *
 */
public class Barrel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private int id;
	
	private float temperature;
	
	public Barrel(final int id, final float temperature) {
		this.id = id;
		this.temperature = temperature;
	}
	
	@ProtoField(number = 1, required = true)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@ProtoField(number = 2)
	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
}
