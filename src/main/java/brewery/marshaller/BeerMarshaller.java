package brewery.marshaller;

import java.io.IOException;

import org.infinispan.protostream.MessageMarshaller;

import brewery.model.Beer;

public class BeerMarshaller implements MessageMarshaller<Beer> {

   @Override
   public String getTypeName() {
      return "brewery.Order.Beer";
   }

   @Override
   public Class<Beer> getJavaClass() {
      return Beer.class;
   }

   @Override
   public Beer readFrom(ProtoStreamReader reader) throws IOException {
	   
		int id = reader.readInt("id");
 
		Beer beer = new Beer(id);

		return beer;
   }

   @Override
   public void writeTo(ProtoStreamWriter writer, Beer beer) throws IOException {
      writer.writeInt("id", beer.getId());
   }
}

