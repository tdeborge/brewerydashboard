package brewery.marshaller;

import java.io.IOException;

import org.infinispan.protostream.MessageMarshaller;

import brewery.model.Barrel;
import brewery.model.Customer;

public class CustomerMarshaller implements MessageMarshaller<Customer> {

   @Override
   public String getTypeName() {
      return "brewery.Order.Customer";
   }

   @Override
   public Class<Customer> getJavaClass() {
      return Customer.class;
   }

   @Override
   public Customer readFrom(ProtoStreamReader reader) throws IOException {
	   
		String firstName = reader.readString("firstName");
		String lastName = reader.readString("lastName");
 
		Customer customer = new Customer(firstName, lastName);

		return customer;
   }

   @Override
   public void writeTo(ProtoStreamWriter writer, Customer Customer) throws IOException {
      writer.writeString("firstName", Customer.getFirstName());
      writer.writeString("lastName", Customer.getLastName());
   }
}

