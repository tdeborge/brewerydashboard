package brewery.marshaller;

import java.io.IOException;

import org.infinispan.protostream.MessageMarshaller;

import brewery.model.OrderTemp;

public class OrderTempMarshaller implements MessageMarshaller<OrderTemp> {

   @Override
   public String getTypeName() {
      return "brewery.OrderTemp";
   }

   @Override
   public Class<OrderTemp> getJavaClass() {
      return OrderTemp.class;
   }

   @Override
   public OrderTemp readFrom(ProtoStreamReader reader) throws IOException {
	   
		int id = reader.readInt("id");
		String firstName = reader.readString("firstName");
		String lastName = reader.readString("lastName");
		int beerId = reader.readInt("beerId");
		int beerQuantity = reader.readInt("beerQuantity");
 
		OrderTemp order = new OrderTemp(id, firstName, lastName, beerId, beerQuantity);

		return order;
   }

   @Override
   public void writeTo(ProtoStreamWriter writer, OrderTemp order) throws IOException {
      writer.writeInt("id", order.getId());
      writer.writeString("firstName", order.getFirstName());
      writer.writeString("lastName", order.getLastName());
      writer.writeInt("beerId", order.getBeerId());
      writer.writeInt("beerQuantity", order.getBeerQuantity());
   }
}

