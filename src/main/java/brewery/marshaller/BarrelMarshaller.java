package brewery.marshaller;

import java.io.IOException;

import org.infinispan.protostream.MessageMarshaller;

import brewery.model.Barrel;

public class BarrelMarshaller implements MessageMarshaller<Barrel> {

   @Override
   public String getTypeName() {
      return "brewery.Barrel";
   }

   @Override
   public Class<Barrel> getJavaClass() {
      return Barrel.class;
   }

   @Override
   public Barrel readFrom(ProtoStreamReader reader) throws IOException {
	   
		int id = reader.readInt("id");
		float temperature = reader.readFloat("temperature");
 
		Barrel barrel = new Barrel(id, temperature);

		return barrel;
   }

   @Override
   public void writeTo(ProtoStreamWriter writer, Barrel barrel) throws IOException {
      writer.writeInt("id", barrel.getId());
      writer.writeFloat("temperature", barrel.getTemperature());
   }
}

