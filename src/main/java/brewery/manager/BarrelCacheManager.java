package brewery.manager;

import java.io.IOException;

import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.marshall.ProtoStreamMarshaller;
import org.infinispan.protostream.FileDescriptorSource;
import org.infinispan.protostream.SerializationContext;

import brewery.helper.Resources;
import brewery.listener.BarrelCacheListener;
import brewery.marshaller.BarrelMarshaller;
import brewery.model.Barrel;

public class BarrelCacheManager {
	
	private static final String BARREL_CACHE_NAME = "barrelcache";
	private static final String PROTOBUF_DEFINITION_RESOURCE = "/proto/barrel.proto";
	
	private RemoteCacheManager remoteCacheManager;
	private RemoteCache<Integer, Barrel> remoteCache;
	
	public BarrelCacheManager () throws IOException {

		remoteCacheManager = Resources.getRemoteCacheContainer();
		remoteCache = remoteCacheManager.getCache(BARREL_CACHE_NAME);
		
		registerMarshallers();
		registerListeners();
	}
	
	private void registerMarshallers() throws IOException {
		SerializationContext ctx = ProtoStreamMarshaller.getSerializationContext(remoteCacheManager);
	    ctx.registerProtoFiles(FileDescriptorSource.fromResources(PROTOBUF_DEFINITION_RESOURCE));
	    ctx.registerMarshaller(new BarrelMarshaller());
		
//		// generate the 'memo.proto' schema file based on the annotations on Memo class and register it with the SerializationContext of the client
//	      ProtoSchemaBuilder protoSchemaBuilder = new ProtoSchemaBuilder();
//	      String protoSchemaFile = protoSchemaBuilder
//	            .fileName(PROTOBUF_DEFINITION_RESOURCE)
//	            .packageName("brewery")
//	            .addClass(Barrel.class)
//	            .build(ctx);
//
//	      // register the schemas with the server too
//	      RemoteCache<String, String> metadataCache = remoteCacheManager.getCache(ProtobufMetadataManagerConstants.PROTOBUF_METADATA_CACHE_NAME);
//	      metadataCache.put(PROTOBUF_DEFINITION_RESOURCE, protoSchemaFile);
//	      String errors = metadataCache.get(ProtobufMetadataManagerConstants.ERRORS_KEY_SUFFIX);
//	      if (errors != null) {
//	         throw new IllegalStateException("Some Protobuf schema files contain errors:\n" + errors);
//	      }
	}
	
	private void registerListeners(){
		remoteCache.addClientListener(new BarrelCacheListener());
	}

	public Barrel getBarrel(int barrel_id) {
		Barrel barrel = (Barrel) remoteCache.get(barrel_id); 
		return barrel;
	}
	
	public void updateBarrel(Barrel barrel) {
		remoteCache.put(barrel.getId(), barrel);
	}
	
	public void clear() {
		remoteCache.clear();
	}
	
	public void stop() {
		remoteCache.stop();
	}
}
