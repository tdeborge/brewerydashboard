package brewery.manager;

import java.io.IOException;

import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.marshall.ProtoStreamMarshaller;
import org.infinispan.protostream.FileDescriptorSource;
import org.infinispan.protostream.SerializationContext;

import brewery.helper.Resources;
import brewery.listener.OrderCacheListener;
import brewery.marshaller.BeerMarshaller;
import brewery.marshaller.BeerQuantityMarshaller;
import brewery.marshaller.CustomerMarshaller;
import brewery.marshaller.OrderMarshaller;
import brewery.marshaller.OrderTempMarshaller;
import brewery.model.Order;
import brewery.model.OrderTemp;

public class OrderCacheManager {
	
	private static final String ORDER_CACHE_NAME = "ordercache";
	private static final String PROTOBUF_DEFINITION_RESOURCE = "/proto/order.proto";
	
	RemoteCache<Integer, Object> remoteCache;
	
	public OrderCacheManager() throws Exception {

		RemoteCacheManager remoteCacheManager = Resources.getRemoteCacheContainer();
		remoteCache = remoteCacheManager.getCache(ORDER_CACHE_NAME);
		
		registerMarshallers(remoteCacheManager);
		registerListeners();
	}
	
	private void registerMarshallers(RemoteCacheManager remoteCacheManager) throws IOException {
		SerializationContext ctx = ProtoStreamMarshaller.getSerializationContext(remoteCacheManager);
	    ctx.registerProtoFiles(FileDescriptorSource.fromResources(PROTOBUF_DEFINITION_RESOURCE));
	    ctx.registerMarshaller(new OrderTempMarshaller());
	    ctx.registerMarshaller(new OrderMarshaller());
	    ctx.registerMarshaller(new CustomerMarshaller());
	    ctx.registerMarshaller(new BeerMarshaller());
	    ctx.registerMarshaller(new BeerQuantityMarshaller());
	}
	
	private void registerListeners(){
		remoteCache.addClientListener(new OrderCacheListener());
	}
    
	public Order getOrder(int order_id) {
		Order order = (Order) remoteCache.get(order_id); 
		return order;
	}
	
	public void orderBeer(Order order) {
		remoteCache.put(order.getId(), order);
	}
	
	public void orderTempBeer(OrderTemp order) {
		remoteCache.put(order.getId(), order);
	}
	public void clear() {
		remoteCache.clear();
	}
	
	public void stop() {
		remoteCache.stop();
	}
}
