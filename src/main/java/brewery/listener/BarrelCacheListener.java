package brewery.listener;

import org.infinispan.client.hotrod.annotation.ClientCacheEntryCreated;
import org.infinispan.client.hotrod.annotation.ClientCacheEntryModified;
import org.infinispan.client.hotrod.annotation.ClientListener;
import org.infinispan.client.hotrod.event.ClientCacheEntryCreatedEvent;
import org.infinispan.client.hotrod.event.ClientCacheEntryModifiedEvent;

@ClientListener
public class BarrelCacheListener {
	
	 @ClientCacheEntryCreated
     public void entryCreated(ClientCacheEntryCreatedEvent<Integer> event) {
		 System.out.println("Add new barrel '" + event.getKey() + "'");
     }

     @ClientCacheEntryModified
     public void entryModified(ClientCacheEntryModifiedEvent<Integer> event) {
    	 System.out.println("Update barrel '" + event.getKey() + "'");
     }
}
